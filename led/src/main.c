/*
 * main.c
 *
 *  Created on: Nov 17, 2021
 *      Author: PC
 */

#include "main.h"

extern volatile uint8_t g_rx_char[16];
const PARSE_PACKET_PROCESS_T packet_process_table[] =
{
    {'1',uart_led_on},
    {'2',uart_led_off},
    {'3',uart_led_blink},
};

void uart_led_on(void)
{
	LED2 = 0U;
}
void uart_led_off(void)
{
	LED2 = 1U;
}
void uart_led_blink(void)
{
	R_Config_CMT0_Start();
	nop();
}
void uart_led_process(uint8_t input)
{
	uint32_t i;
	uint32_t arr_size = sizeof(packet_process_table)/sizeof(packet_process_table[0]);
	for (i = 0;i < arr_size; i++)
	{
		if(strcmp((char *)packet_process_table[i].packet_str,(char *)input) == 0)
		{
			packet_process_table[i].func_ptr();
			break;
		}
	}
}
void uart_main(void)
{
	R_Config_SCI1_Start();
	while (1U)
	{
		R_Config_SCI1_Serial_Receive((uint8_t *)&g_rx_char,16);
		R_BSP_SoftwareDelay(1, BSP_DELAY_SECS);
		uart_led_process(g_rx_char);
	}
}

