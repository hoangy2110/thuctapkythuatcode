/*
 * gps.h
 *
 *  Created on: Nov 24, 2021
 *      Author: PC
 */

#ifndef DRIVER_GPS_GPS_H_
#define DRIVER_GPS_GPS_H_

#include "r_smc_entry.h"
#include <string.h>
#include <stdlib.h>

typedef struct parse_msg_gprmc{
	uint8_t			*field_id;
	uint8_t 		*field_utc;
	uint8_t			*field_status;
	uint8_t			*field_lat;
	uint8_t 		*field_NS_indi;
	uint8_t			*field_long;
	uint8_t			*field_EW_indi;
	uint8_t 		*field_speed;
	uint8_t			*field_course;
	uint8_t			*field_data;
	uint8_t 		*field_mag;
	uint8_t			*field_ES;
	uint8_t			*field_mode;
	uint8_t 		*field_checksum;
}PARSE_MSG_GPRMC;

int split (uint8_t *str, uint8_t c, uint8_t ***arr);

void process_gps (void);

void parse_msg_gps(uint8_t *GPRMC_MSG, uint8_t *GPACC_MSG, uint8_t *MSG_GPS);

void parse_msg_gprmc(uint8_t *GPRMC_MSG, PARSE_MSG_GPRMC msg_rmc);

#endif /* DRIVER_GPS_GPS_H_ */
