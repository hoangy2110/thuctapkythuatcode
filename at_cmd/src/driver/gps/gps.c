/*
 * gps.c
 *
 *  Created on: Nov 24, 2021
 *      Author: PC
 */

#include "gps.h"

volatile uint8_t *Config_PMTK =  "$PMTK314,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*29\r\n"; //cau hinh pmtk
volatile uint8_t *msg_gprmc; // ban tin gprmc
volatile uint8_t *rmc_id;
volatile PARSE_MSG_GPRMC parser_msg_gprmc;
volatile uint8_t g_msg[256];

void process_gps (void)
{
	uint8_t *msg;
	uint8_t **msg_gps = NULL;
	uint8_t **gprmc = NULL;
	R_Config_SCI1_Serial_Send((uint8_t *)Config_PMTK, 52);
	R_BSP_SoftwareDelay(2, BSP_DELAY_SECS);
	while(1)
	{
		int count_1 = 0;
		int count_2 = 0;
		int i = 0;

		R_Config_SCI1_Serial_Receive((uint8_t *)&g_msg, 256);
		msg = (uint8_t *)(g_msg);

		count_1 = split(msg,'\n',&msg_gps);
		msg_gprmc = msg_gps[0];

		count_2 = split(msg_gps[0],',',&gprmc);
		parser_msg_gprmc.field_id = gprmc[0];

		R_Config_SCI5_Serial_Send((uint8_t *)msg_gprmc,52);

		for(i = 0; i < count_1; i++)
		{
			free(msg_gps[i]);
		}
		free(msg_gps);
		for(i = 0; i < count_2; i++)
		{
			free(gprmc[i]);
		}
		free(gprmc);

		R_BSP_SoftwareDelay(1000, BSP_DELAY_MILLISECS);
	}
}

int split (uint8_t *str, uint8_t c,uint8_t ***arr)
{
    int count = 1;
    int token_len = 1;
    int i = 0;
    uint8_t *p;
    uint8_t *t;

    p = str;
    while (*p != '\0')
    {
        if (*p == c)
            count++;
        p++;
    }

    *arr = (uint8_t**) malloc(sizeof(uint8_t*) * count);

    p = str;
    while (*p != '\0')
    {
        if (*p == c)
        {
            (*arr)[i] = (uint8_t*) malloc( sizeof(uint8_t) * token_len );

            token_len = 0;
            i++;
        }
        p++;
        token_len++;
    }
    (*arr)[i] = (uint8_t*) malloc( sizeof(uint8_t) * token_len );

    i = 0;
    p = str;
    t = ((*arr)[i]);
    while (*p != '\0')
    {
        if (*p != c && *p != '\0')
        {
            *t = *p;
            t++;
        }
        else
        {
            *t = '\0';
            i++;
            t = ((*arr)[i]);
        }
        p++;
    }
    return count;
}
