/***********************************************************************
*
*  FILE        : at_cmd.c
*  DATE        : 2021-11-17
*  DESCRIPTION : Main Program
*
*  NOTE:THIS IS A TYPICAL EXAMPLE.
*
***********************************************************************/
//#include "r_smc_entry.h"
#include "sim868.h"
#include <string.h>
#include "./driver/gps/gps.h"

extern volatile uint8_t g_rx_sim[256];
#define LED2 (PORT5.PODR.BIT.B3)
typedef void (*PACKET_FUNC_PTR)(void);
typedef struct parse_uart_process{
    uint8_t             *field_id;
    uint8_t 			*data;
    PACKET_FUNC_PTR     func_ptr;
}PARSE_PACKET_PROCESS_T;

void main(void);

void main(void)
{
	R_Config_SCI1_Start();
	R_Config_SCI5_Start();
	process_gps();
}
